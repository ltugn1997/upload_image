import 'package:flutter/foundation.dart';
import 'dart:io';

class ScanImage {
  final String id;
  final File image;

  ScanImage({@required this.id, @required this.image});
}
