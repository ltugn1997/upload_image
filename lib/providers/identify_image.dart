import 'package:flutter/foundation.dart';

import 'package:checking_app/models/scan_image.dart';

class IdentifyImage with ChangeNotifier {
  List<ScanImage> _items = [];

  List<ScanImage> get items {
    return [..._items];
  }

  void checkImage(String Image) {
    print(Image);
  }
}
