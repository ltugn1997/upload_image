import 'package:flutter/material.dart';
import 'package:checking_app/widgets/image_input.dart';

class AddPictureScreen extends StatefulWidget {
  static const routeName = '/add-place';
  @override
  _AddPictureScreenState createState() => _AddPictureScreenState();
}

class _AddPictureScreenState extends State<AddPictureScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scan Your Face'),
      ),
      body: ImageInput(),
    );
  }
}
