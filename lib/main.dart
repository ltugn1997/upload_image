import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:checking_app/providers/identify_image.dart';
import 'package:checking_app/screen/add_picture_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: IdentifyImage(),
      child: MaterialApp(
        title: 'Scan Your Face',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
          accentColor: Colors.amber,
        ),
        home: AddPictureScreen(),
        routes: {
          AddPictureScreen.routeName: (ctx) => AddPictureScreen(),
        },
      ),
    );
  }
}
